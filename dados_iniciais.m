%%
%Dados iniciais para controle PI sendo aplicado em theta_off
%%
%Valores iniciais do MRV 12/8
clc;
load('dados_para_simular_9A_1251.mat');
%load('dadosmaggrv.mat');
Ts=5e-6; % Periodo de amostragem
set(0,'DefaultFigureColor','w');

Tc=1/40e3; %periodo de amostragem do controlador
%% Discretiza��o do controlador de theta_off
Num_PI_theta_off = [0.007 45];     
Den_PI_theta_off = [1 0];      
PI_theta_off = tf(Num_PI_theta_off,Den_PI_theta_off);
PI_theta_off_Discreto = c2d(PI_theta_off,Tc,'tustin');
[Num_Discreto_theta_off,Den_Discreto_theta_off] = tfdata(PI_theta_off_Discreto,'v');
tf_Z_theta_off = filt(Num_Discreto_theta_off,Den_Discreto_theta_off,Tc);
%% Discretiza��o do controlador de torque
Num_PI_Torque = [0.007 0.065];     
Den_PI_Torque = [1 0];      
PI_Torque = tf(Num_PI_Torque,Den_PI_Torque);
PI_Torque_Discreto = c2d(PI_Torque,Tc,'tustin');
[Num_Discreto_Torque,Den_Discreto_Torque] = tfdata(PI_Torque_Discreto,'v');
tf_Z_Torque = filt(Num_Discreto_Torque,Den_Discreto_Torque,Tc);

%% Inicializa��o das constantes da m�quina
flux_max_mod=0.208;      %para tabela motor maquina de lavar
Fmax=flux_max_mod; 
R_mod=2.2;            %para tabela motor maquina de lavar
J=0.004;            %para tabela motor maquina de lavar
B=0.002;           %para tabela motor maquina de lavar
Rs=R_mod;
Lmin = 6.553e-3;    %para tabela motor maquina de  lavar
Lmax = 28.525e-3;    %para tabela motor maquina de lavar
Nph = 3;
Nr = 8;
pos_init = 10;

%% Inicializa��o das cosnstantes externas
TL= 2.0;
Vdc = 80;       %Tens�o para motor maquina de lavar

%parametros rotor
PosMax=45;
pospolorotor=[0 -15 -30];

theta_off_max = 42.5; %Angulo de desligamento
theta_off_min = 23;
theta_off = theta_off_max;
theta_on = 23;    %Angulo de acionamento

%Banda de torque
thss = -1.0;
thsm = -0.25; %tem que ser um valor negativo
thim = 0.25; %tem que ser um valor positivo
thii = 1.0;

Imax = 6.5;
Tmax = 6.5;   %motor maq lavar
Ig_hist = 0.3; %Largura de banda de histerese de corrente

afde=sim('Discreto_MRV_completo_chopping_current_hysteresis.slx');


