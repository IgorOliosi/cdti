%%
%Dados iniciais para controle PI sendo aplicado em theta_off e na
%corrente(histese?)
%%
%Valores iniciais do MRV 12/8
clc;
load('dados_para_simular_9A_1251.mat');
%load('dadosmaggrv.mat');
Ts=5e-6; % Periodo de amostragem
set(0,'DefaultFigureColor','w');

Tc=1/40e3; %periodo de amostragem do controlador
%% Discretiza��o do controlador de theta_off
Num_PI_theta_off = [7 0.4];  
Den_PI_theta_off = [1 0];      
PI_theta_off = tf(Num_PI_theta_off,Den_PI_theta_off);
PI_theta_off_Discreto = c2d(PI_theta_off,Tc,'tustin');
[Num_Discreto_theta_off,Den_Discreto_theta_off] = tfdata(PI_theta_off_Discreto,'v');
tf_Z_theta_off = filt(Num_Discreto_theta_off,Den_Discreto_theta_off,Tc);
%% Discretiza��o do controlador de corrente
Num_PI_Corrente = [7 0.65];     
Den_PI_Corrente = [1 0];      
PI_Corrente = tf(Num_PI_Corrente,Den_PI_Corrente);
PI_Corrente_Discreto = c2d(PI_Corrente,Tc,'tustin');
[Num_Discreto_Corrente,Den_Discreto_Corrente] = tfdata(PI_Corrente_Discreto,'v');
tf_Z_corrente = filt(Num_Discreto_Corrente,Den_Discreto_Corrente,Tc);
%% Inicializa��o das constantes da m�quina
flux_max_mod=0.22;      %para tabela motor AZ1k5
Fmax=flux_max_mod;  
R_mod=0.3;            %para tabela motor AZ1k5
J=0.002;            %para tabela motor AZ1k5
B=0.0098;           %para tabela motor AZ1k5

Rs=R_mod;
Lmin = 1.675e-3;    %para tabela motor AZ1k5
Lmax = 13.88e-3;    %para tabela motor AZ1k5
Nph = 3;
Nr = 8;
pos_init = 10;

%% Inicializa��o das cosnstantes externas
TL=0;
Vdc = 80;       %Tens�o para motor az1k5
PosMax=45;
pospolorotor=[0 -15 -30];
theta_o = 21.5; %Angulo que come�a a sobreposi��o 24

% theta_max = 44.5; %Angulo de desligamento maximo
theta_off_max = 40; %Angulo de desligamento
theta_off_min = 26;
theta_on_max = 23.5;
theta_on_min = 18;
theta_on = 20;    %Angulo de acionamento
theta_off_add=20;  %Angulo somado � sa�da do PI
%theta_ov = 6;     %Angulo de comutacao
theta_p = 45;     %Angulo de pitch

Imax = 25; %motor azul
Ig_hist = 0.3; %Largura de banda de histerese de corrente

open('Discreto_MRV_completo_chopping_current_hysteresis.slx')
%afde=sim('Discreto_MRV_completo_chopping_current_hysteresis.slx');


